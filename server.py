from bottle import *

# Index/Home page
# Copy these to create a new route
@route('/')
def index():
    return template(
        'pages/new/index.html', 
        username="Login", # Will be different on production
        title="Home", 
        description="Home Page", 
        menu={'screeners': [], 'sectors': []} # Will be different on production
    )

# Test React
@route('/react')
def react():
    return template(
        'build/index.html', 
        username="Login", 
        title="Home", 
        description="Home Page", 
        menu={'screeners': [], 'sectors': []}
    )

# Test feature page
@route('/feature')
def product_page():
    return template(
        'pages/feature/feature.html', 
        username="Username", 
        title="Feature", 
        description="Feature Page", 
        menu={'screeners': [], 'sectors': []} 
    )

# Feature2 page
@route('/feature2')
def feature2_page():
    return template(
        'pages/feature2/feature2.html', 
        username="Username", 
        title="Feature", 
        description="Feature Page", 
        menu={'screeners': [], 'sectors': []} 
    )

# isscanner page
@route('/iscanner')
def feature2_page():
    return template(
        'pages/about-iscanner/index.html', 
        username="Username", 
        title="Feature", 
        description="Feature Page", 
        menu={'screeners': [], 'sectors': []} 
    )

# Feature2 page
@route('/remisierbot')
def feature2_page():
    return template(
        'pages/about-remisierbot/index.html', 
        username="Username", 
        title="Feature", 
        description="Feature Page", 
        menu={'screeners': [], 'sectors': []} 
    )

# Serve static folder
@route('/feature/<filepath:path>')
def serve_product_files(filepath):
    if (".html" not in filepath):
        return static_file(filepath, root='pages/feature')
    else:
        abort(404)

# Serve static folder
@route('/iscanner/about/<filepath:path>')
def serve_product_files(filepath):
    if (".html" not in filepath):
        return static_file(filepath, root='pages/about-iscanner')
    else:
        abort(404)

# Serve static folder
@route('/remisierbot/about/<filepath:path>')
def serve_product_files(filepath):
    if (".html" not in filepath):
        return static_file(filepath, root='pages/about-remisierbot')
    else:
        abort(404)


# Serve dist folder
@route('/build/<filepath:path>')
def serve_dist_files(filepath):
    return static_file(filepath, root='build/')

# Serve static folder
@route('/<filepath:path>')
def serve_static_files(filepath):
    return static_file(filepath, root='static/')
    
run(reloader=True, host='localhost', port=8082)